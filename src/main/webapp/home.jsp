<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
Create:
	<form action="person" method="post">
		<input type="text" name="id" />
		<input type="text" name="name" />
		<input type="email" name="email" />
		<input type="submit" />
	</form>
	<br/>
Get:
	<form action="person" method="get">
		<input type="text" name="id" />
		<input type="submit" />
	</form>
	<br/>
Update:
	<form action="person" method="post">
		<input type="text" name="id" />
		<input type="text" name="name" />
		<input type="email" name="email" />
		<input type="submit" />
	</form>
	<br/>
	<br/>
	${person.id} <br/>
	${person.name }
</body>
</html>