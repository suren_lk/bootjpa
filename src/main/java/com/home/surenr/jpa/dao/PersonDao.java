package com.home.surenr.jpa.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.home.surenr.jpa.model.Person;

public interface PersonDao extends CrudRepository<Person, Integer>{
	
	public List<Person> findByName(String name);
	
	@Query("from Person order by id")
	public List<Person> findByIdSorted();
}
