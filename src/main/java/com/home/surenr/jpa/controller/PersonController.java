package com.home.surenr.jpa.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.home.surenr.jpa.dao.PersonDao;
import com.home.surenr.jpa.model.Person;

@Controller
public class PersonController {

	@Autowired
	PersonDao personDao;
	
	@RequestMapping("/")
	public String home() {
		return "home.jsp";
	}
	
	@PostMapping("/person")
	public String addPerson(Person person) {
		Person origPerson = personDao.findById(person.getId()).orElse(null);
		if(origPerson == null) {
			personDao.save(person);
		}else {
			origPerson = person;
			personDao.save(origPerson);
		}
		return "home.jsp";
	}
	
	@GetMapping("/person")
	public ModelAndView getPerson(@RequestParam int id) {
		ModelAndView mv = new ModelAndView();
		mv.setViewName("home.jsp");
		Person person = personDao.findById(id).orElse(new Person());
		System.out.println(personDao.findByName("Surendran Ramasamy").get(0).getEmail());
		
		System.out.println(personDao.findByIdSorted());
		
		mv.addObject(person);
		return mv;
	}
	
	@GetMapping("/person/{id}")
	@ResponseBody
	public Person getPersonById(@PathVariable int id) {
		Person person = personDao.findById(id).orElse(new Person());
		return person;
	}
	
	@PatchMapping("/person")
	public ModelAndView updatePerson(Person person) {
		Person origPerson = personDao.findById(person.getId()).orElse(person);
		origPerson = person;
		personDao.save(origPerson);
		ModelAndView mv = new ModelAndView();
		mv.setViewName("home.jsp");
		mv.addObject(origPerson);
		return mv;
	}

}
